# ProblemGen
## What is it?
ProblemGen is a project of mine I made a while back.  It uses a mini plugin system to generate problems that can be used for math tests, along with solutions.  It is pretty easy to add new plugins in, and everything is done with calls to other programs, kinda like a cgi server.  It puts the questions and answers into two html files that can be printed out.

## Usage

This program is broken down into about 3 interfaces (written in TK).  It is reccomended that you pop the edit and plugin menues, as its nice to have these out on the side.

### Main Editing Area
![main editing area picture](http://i.imgur.com/IRTvuOx.png "Yes, I'm serious about popping out those extra windows.")

To add a question, hit edit>add question.  It will add a question in the format blank.py,1.  Blank.py is the default “question”.  The number after the comma is how many of the question to add.

Delete questions will delete the question set you have selected.  Be careful with this, you have to re-select a new piece in the box every time.

### Question Editing
![Question Editing Menue](http://i.imgur.com/XxtcYi7.png "Editing each question")

What ever you have selected on the main question menu can be changed using the edit box.  The slider sets the number of questions per set (maximum ten, minimum none), and you can select the plugin to use below it.  Note that this screen may look very different on windows machines, due to how tk works.

### Plugins
![Plugin menue](http://i.imgur.com/x48iuuO.png "Adding plugins.")

View plugins only brings up a box that shows the currently installed plugins.  You can see the raw file yourself as plugs.ini.  Add plugin takes a py file, copies it to the /plugs/ directory, and adds the file name to plugs.ini.  Oh, and as a note, you can technically put anything in those files.  Making an infectious plugin is possible.

## How does it work

A quick disclaimer, this program is most definitely full of holes that I have not found yet.  I can assure you that this is in no way bullet proof and you will find errors where they probably exist.  I have not put in places to sanitize inputs of [insert part of project here] so there are many ways you can go wrong with it.

Now, on the topic of the program itself.  The actual core of this is only about 86 lines.  The program core simply calls the plugins it needs via the command line, and then pipes the responces back to a string.  The first string is the test questions, and the second one holds the solutions.  After being generated, these then get written into two html documents.

Projects can be saved and loaded through a simple system that explodes the file based on newlines and commas.  These simply have the name of the program and the number of questions that need to be generated.  Originally it used pickle, but some variable glitchyness caused me to ditch it.